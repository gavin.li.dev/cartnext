

function BasicGallery({src}) {
    return (
        <div>
            <img className="w-full" src={src} alt="" />
        </div>
    );
}

export default BasicGallery;