function Header() {
    return (
        <>
            <meta name="viewport" content="width=device-width, initial-scale=1.0"></meta>
            <title>Cart Next: another cool open source shop written with NextJS and accept Lightning Payment</title>
        </>
    );
}

export default Header;