import BasicGallery from "components/Gallery/Basic";
import BuyButton from "components/Button/Buy";

function ProductPage() {
    const product = {
        id: "123qwe456asd",
        slug: "",
        name: "Marble Explorer Swingback Wall Marble Run",
        sku: "LG503",
        price: "48.8",
        images: [
            'https://img.staticdj.com/fd46402816b3af9f4e7c027f188d20a9_1080x.jpeg'
        ]
    }


    return (
        <section className="container m-auto my-4 grid grid-cols-2">
            <BasicGallery src={product.images[0]} />
            <div className="px-8 flex flex-col justify-between">
                <div>
                    <h1>{product.name}</h1>
                    <span className="sub-header">{product.sku}</span>
                    <div>${product.price}</div>
                </div>

                <BuyButton pid={product.id}/>
            </div>
        </section>
    );
}

export default ProductPage;