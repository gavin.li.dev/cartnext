function CategoryPage({ params }) {
    return (
        <div>
            Category Page: {params.slug}
        </div>
    );
}

export default CategoryPage;