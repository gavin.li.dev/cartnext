import { MongoClient, Db } from "mongodb";

let dbClient: MongoClient;
let coinDb: Db;

export async function getClient() {
    if(dbClient) {
        return dbClient;
    }

    const dbURL = process.env.DATABASE_URL;
    if(!dbURL) {
        throw new Error('Must have a database connection string setup in environment');
    }

    dbClient = await MongoClient.connect(dbURL);
    return dbClient;
}

export async function getDb() {
    if(coinDb) {
        return coinDb;
    }

    const dbClient = await getClient();
    coinDb = dbClient.db('coin');

    return coinDb;
}
